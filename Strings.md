---
jupyter:
  jupytext:
    formats: ipynb,py:light,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.6
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

# Working with strings


## Removing prefixes

```python
def remove_prefix(text,prefix):
    ''' Remove the prefix `prefix` from `text`
        
        Paremters:
            text                   string in which to search for substrings
            prefix                 substring to remove from `text`
        Return:
            `text` without `prefix` if `text` starts with the substring `prefix`, otherwise `text`
        
        >>> remove_prefix('prefix','pre')
        'fix'
        >>> remove_prefix('prefix','post')
        'prefix'
        >>> remove_prefix('postfix','pre')
        'postfix'
        >>> remove_prefix('postfix','post')
        'fix'
    '''
    text_str = str(text)
    if text_str.startswith(prefix):
        return text_str[len(prefix):]
    return text_str  # or whatever
```

## Removing suffixes

```python
def remove_suffix(text,suffix):
    ''' Remove the suffix `suffix` from `text`
        
        Paremters:
            text                   string in which to search for substrings
            prefix                 substring to remove from `text`
        Return:
            `text` without `suffix` if `text` ends with the substring `suffix`, otherwise `text`
        
        >>> remove_suffix('abcdc.com','.com')
        'abcdc'
        >>> remove_suffix('abcdc.com','.de')
        'abcdc.com'
        >>> remove_suffix('abcdc.de','.com')
        'abcdc.de'
        >>> remove_suffix('abcdc.de','.de')
        'abcdc'
    '''
    if text.endswith(suffix):
        return(text[:-len(suffix)])
    return(text)
```

## Pythonic

```python
###########################
# On Python 3.9 and newer #
###########################

url = 'abcdc.com'
print(url.removesuffix('.com'))    # Returns 'abcdc'
print(url.removeprefix('abcdc.'))  # Returns 'com'

```

## Appending the systems Path separator

```python
from pathlib import  Path

print(f"{remove_suffix(str(Path('folder') / 'subfolder'), 'subfolder')=}")
```

## Unit Tests

```python
import doctest
doctest.testmod(verbose=True)
```
