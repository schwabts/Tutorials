---
jupyter:
  jupytext:
    formats: ipynb,py:light,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.6
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

<!--bibtex

@Article{PER-GRA:2007,
  Author    = {P\'erez, Fernando and Granger, Brian E.},
  Title     = {{IP}ython: a System for Interactive Scientific Computing},
  Journal   = {Computing in Science and Engineering},
  Volume    = {9},
  Number    = {3},
  Pages     = {21--29},
  month     = may,
  year      = 2007,
  url       = "http://ipython.org",
  ISSN      = "1521-9615",
  doi       = {10.1109/MCSE.2007.53},
  publisher = {IEEE Computer Society},
}

-->


# Using Calico Document Tools and Bibtex


This document `README.ipynb` **must not be modified**.


## For Testing


First duplicate this example and modify the duplicate.

<img src="images/2022-05-15 Testing Calico Document Tools and Bibtex.png"/>

<img src="images/2022-05-15 Duplicating.png"/>

<img src="images/2022-05-15 Only Test With Duplicate.png"/>


## Generating References


Format your citations as links like `[cite](#cite-BIBTEX_LABEL)`, i.e. `[cite](#cite-PER-GRA:2007)` in the following example.


Thus, if you want to cite IPython, then you could use that 
citation [cite](#cite-PER-GRA:2007)


Prepared like this simply click on the button:

<img src="images/2022-05-15 Generating References.png"/>

```python

```
